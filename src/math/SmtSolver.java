package math;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import com.sun.org.apache.xml.internal.resolver.readers.XCatalogReader;

import variable.Helper;

public class SmtSolver {
	// list expression.
	private ArrayList<Expression> listExpression;
	int numLine = 0;
	public long totalTime=0;

	public SmtSolver() {
		listExpression = new ArrayList<Expression>();
	}

	public void addExpression(Expression e) {
		listExpression.add(e);
	}

	public ArrayList<Expression> getListExpression() {
		return listExpression;
	}

	public boolean isEmpty() {
		return listExpression.isEmpty();
	}

	public boolean run() throws ParseFormatException, IOException,
			ContradictionException, TimeoutException, InterruptedException {
		ISolver solver = SolverFactory.newDefault();
		numLine = 0;
		solver.setTimeout(3600);
		solver.setDBSimplificationAllowed(true);
		Reader reader = new DimacsReader(solver);
		ArrayList<Variable> list = Helper.getListVariable();
		String result = "p cnf";
		for (Expression e : listExpression) {
			result += e.getCnf();
		}
		for (Expression e : listExpression) {
			numLine += e.numberLine;
		}
		result += getCnfOfNumberVariable();
		result = result.replace("p cnf", "p cnf "
				+ (Helper.id * Helper.BIT_NUMBER + Helper.BIT_NUMBER) + " "
				+ numLine);
		for (Variable v : Helper.varList) {
			System.out.println("Id: "+v.id+ " name:"+v.name);
		}
		Helper.fileContent = result;
		PrintWriter writer = new PrintWriter("filecnf.txt", "UTF-8");
		writer.println(result);
		writer.close();
		long startTime = System.currentTimeMillis();
		IProblem problem = reader.parseInstance("filecnf.txt");
		long endTime   = System.currentTimeMillis();
		totalTime = endTime - startTime;
		System.out.println("totaltime:"+totalTime);
		
		if (problem.isSatisfiable()) {
			System.out.println("Satisfiable !");
			System.out.println(reader.decode(problem.model()));
			result = reader.decode(problem.model());
			Scanner sc = new Scanner(result);
			ArrayList<Integer> array = new ArrayList<Integer>();
			while (sc.hasNext()) {
				int a = sc.nextInt();
				if (a == 0)
					break;
				array.add(a);
				System.out.println(a);
			}
			Helper.resultList = array;
			readValueFromResult(array);
			for (Variable v : Helper.varList) {
				System.out.println("Id: "+v.id+ " name:"+v.name+ " "+v.binaryValue+" "+ v.value);
			}
			sc.close();
			return true;
		} else {
			System.out.println("Unsatisfiable !");
			return false;
		}

	}

	private void readValueFromResult(ArrayList<Integer> array) {
		for (Variable v : Helper.varList) {
			if (v.isVariable()) {
				String value = "";
				for (int i = 0; i < array.size(); i++) {
					int current = array.get(i);
					
					if (((current-1) / Helper.BIT_NUMBER) == v.id&&current>0) {
						value = "1" + value;
					} else if ((current+1) / Helper.BIT_NUMBER == -v.id&&current<0) {
						value = "0" + value;
					}
				}
				v.value = Integer.parseInt(value, 2);
				System.out.println("bien" + v.name + " " + v.value + value);
			}
			if (v.isadd) {
				String value = "";
				for (int i = 0; i < array.size(); i++) {
					int current = array.get(i);
					if (((current-1) / Helper.BIT_NUMBER) == v.id&& current>0) {
						value = "1" + value;
					} else if ((current+1) / Helper.BIT_NUMBER == -v.id&& current<0) {
						value = "0" + value;
					}
				}
				System.out.println(value);
				v.binaryValue = value;
			}
		}

	}

	private String getCnfOfNumberVariable() {
		String result = "";
		for (Variable v : Helper.varList) {
			if (v.isNumber()) {
				int value = v.getValue();
				String binaryValue = Utils.DecimalToBinary("" + value);
				for (int i = Helper.BIT_NUMBER; i > 0; i--) {
					if (Integer.parseInt("" + binaryValue.charAt(i - 1)) == 1)
						result += "\n"
								+ (v.id * Helper.BIT_NUMBER + Helper.BIT_NUMBER- i+1) + " 0";
					else
						result += "\n-"
								+ (v.id * Helper.BIT_NUMBER + Helper.BIT_NUMBER- i+1) + " 0";
				}
				numLine += Helper.BIT_NUMBER;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		SmtSolver s = new SmtSolver();
		Expression e = new Expression("(a+d)=5");
		s.addExpression(e);
//		 e = new Expression("b=5");
//		 s.addExpression(e);
		try {
			s.run();
		} catch (Exception a) {
			a.printStackTrace();
		}

	}

}
