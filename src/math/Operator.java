package math;


public class Operator extends MathObject {
	public Operator(String op) {
		this.name = op;
	}
	// List of Operators
	public static Operator[] list = {new Operator("+"), new Operator("-"), new Operator("*"), 
		new Operator("/"), new Operator("^"),  new Operator("("),  new Operator(")")};
	
	public static void resetOp() {
		list = new Operator[7];
		list[0] = new Operator("+");
		list[1] = new Operator("-");
		list[2] = new Operator("*");
		list[3] = new Operator("/");
		list[4] = new Operator("^");
		list[5] = new Operator("(");
		list[6] = new Operator(")");
	}
}