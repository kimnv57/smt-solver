package math;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.Reader;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;

import variable.Helper;

public class Expression {
	String expression = "";
	public ArrayList<MathObject> content1;
	public ArrayList<MathObject> postfix1;
	public ArrayList<MathObject> content2;
	public ArrayList<MathObject> postfix2;
	public int numberLine = 0;
	private String resultString;

	public Expression(String input) {
		expression = input;
		// input = Utils.toOneExpression(input);
		String ar[] = input.split("=");
		String input1 = "(" + ar[0] + ")";
		String input2 = "(" + ar[1] + ")";
		content1 = new ArrayList<MathObject>(0);
		content2 = new ArrayList<MathObject>(0);
		String temp = "";
		for (int i = 0; i < input1.length(); i++)
			switch (input1.charAt(i)) {
			case ' ': {
				if (!temp.equals(""))
					content1.add(Helper.getVariable(temp));
				temp = "";
				break;
			}
			case '+': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[0]);
				temp = "";
				break;
			}
			case '-': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[1]);
				temp = "";
				break;
			}
			case '*': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[2]);
				temp = "";
				break;
			}
			case '/': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[3]);
				temp = "";
				break;
			}
			case '^': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[4]);
				temp = "";
				break;
			}
			case '(': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[5]);
				temp = "";
				break;
			}
			case ')': {
				content1.add(Helper.getVariable(temp));
				content1.add(Operator.list[6]);
				temp = "";
				break;
			}
			default: {
				temp += input1.charAt(i);
				break;
			}
			}

		temp = "";
		for (int i = 0; i < input2.length(); i++)
			switch (input2.charAt(i)) {
			case ' ': {
				if (!temp.equals(""))
					content2.add(Helper.getVariable(temp));
				temp = "";
				break;
			}
			case '+': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[0]);
				temp = "";
				break;
			}
			case '-': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[1]);
				temp = "";
				break;
			}
			case '*': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[2]);
				temp = "";
				break;
			}
			case '/': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[3]);
				temp = "";
				break;
			}
			case '^': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[4]);
				temp = "";
				break;
			}
			case '(': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[5]);
				temp = "";
				break;
			}
			case ')': {
				content2.add(Helper.getVariable(temp));
				content2.add(Operator.list[6]);
				temp = "";
				break;
			}
			default: {
				temp += input2.charAt(i);
				break;
			}
			}
		postfix();
	}

	public void postfix() {
		postfix1 = new ArrayList<MathObject>(0);
		ArrayList<MathObject> stack = new ArrayList<MathObject>(0);
		MathObject temp;
		for (int i = 0; i < content1.size(); i++) {
			temp = content1.get(i);
			if (temp instanceof Variable)
				postfix1.add(temp); // variable, add to result
			else {
				if (temp == Operator.list[5]) // left parentheses
					stack.add(temp);
				else if (temp == Operator.list[6]) { // right parentheses
					while (stack.get(stack.size() - 1) != Operator.list[5]) {
						postfix1.add(stack.get(stack.size() - 1));
						stack.remove(stack.size() - 1);
						if (stack.isEmpty())
							return; // left parentheses not found
					}
					stack.remove(stack.size() - 1); // remove corresponding left
													// parentheses
				} else if (!stack.isEmpty()) {
					int prec1 = 1, prec2 = 0; // prec2 is for stack's, so it may
												// be parentheses
					MathObject temp2 = stack.get(stack.size() - 1);
					for (int j = 0; j < 5; j++) {
						if (temp == Operator.list[j])
							prec1 = j / 2 + 1;
						if (temp2 == Operator.list[j])
							prec2 = j / 2 + 1;
						// + - are 1, * / are 2, ^ are 3
					}
					if (prec1 <= prec2 && prec1 != 3) {
						postfix1.add(temp2);
						stack.remove(stack.size() - 1);
					}
					stack.add(temp);
				} else
					stack.add(temp);
			}
		}
		// pop all stack into postfix
		while (!stack.isEmpty()) {
			postfix1.add(stack.get(stack.size() - 1));
			stack.remove(stack.size() - 1);
		}
		for (int i = 0; i < postfix1.size(); i++) {
			if (postfix1.get(i).name == "") {
				postfix1.remove(i);
				i--;
			}
		}
		for (int i = 0; i < postfix1.size(); i++) {
			System.out.println(postfix1.get(i).name);
		}

		for (int i = 0; i < postfix1.size(); i++) {
			if (postfix1.get(i) instanceof Variable)
				System.out.println(postfix1.get(i).id + "  "
						+ postfix1.get(i).name);
		}

		postfix2 = new ArrayList<MathObject>(0);
		stack = new ArrayList<MathObject>(0);
		for (int i = 0; i < content2.size(); i++) {
			temp = content2.get(i);
			if (temp instanceof Variable)
				postfix2.add(temp); // variable, add to result
			else {
				if (temp == Operator.list[5]) // left parentheses
					stack.add(temp);
				else if (temp == Operator.list[6]) { // right parentheses
					while (stack.get(stack.size() - 1) != Operator.list[5]) {
						postfix2.add(stack.get(stack.size() - 1));
						stack.remove(stack.size() - 1);
						if (stack.isEmpty())
							return; // left parentheses not found
					}
					stack.remove(stack.size() - 1); // remove corresponding left
													// parentheses
				} else if (!stack.isEmpty()) {
					int prec1 = 1, prec2 = 0; // prec2 is for stack's, so it may
												// be parentheses
					MathObject temp2 = stack.get(stack.size() - 1);
					for (int j = 0; j < 5; j++) {
						if (temp == Operator.list[j])
							prec1 = j / 2 + 1;
						if (temp2 == Operator.list[j])
							prec2 = j / 2 + 1;
						// + - are 1, * / are 2, ^ are 3
					}
					if (prec1 <= prec2 && prec1 != 3) {
						postfix2.add(temp2);
						stack.remove(stack.size() - 1);
					}
					stack.add(temp);
				} else
					stack.add(temp);
			}
		}
		// pop all stack into postfix
		while (!stack.isEmpty()) {
			postfix2.add(stack.get(stack.size() - 1));
			stack.remove(stack.size() - 1);
		}
		for (int i = 0; i < postfix2.size(); i++) {
			if (postfix2.get(i).name == "") {
				postfix2.remove(i);
				i--;
			}
		}
		for (int i = 0; i < postfix2.size(); i++) {
			System.out.println(postfix2.get(i).name);
		}

		for (int i = 0; i < postfix2.size(); i++) {
			if (postfix2.get(i) instanceof Variable)
				System.out.println(postfix2.get(i).id + "  "
						+ postfix2.get(i).name);
		}

	}

	public ArrayList<MathObject> getStack1() {
		return postfix1;
	}

	public ArrayList<MathObject> getStack2() {
		return postfix2;
	}

	public String toString() {
		// String result = "";
		// for(MathObject i:content)
		// result += i.name + ' ';
		return expression;
	}

	public String toPostfixString() {
		String result = "";
		for (MathObject i : postfix1)
			result += i.name + ' ';
		result += "=";
		for (MathObject i : postfix2)
			result += i.name + ' ';
		return result;
	}
	//add two mathobject, create resultstring and return result math object.
	public MathObject add(MathObject v1,MathObject v2) {
		Variable vresult = Helper.getVariable("ResultOf ("
				+ v2.name + "+"
				+ v1.name + ")");
		Variable vcarry = Helper.getVariable("CarryOf ("
				+ v2.name + "+"
				+ v1.name + ")");
		vresult.isadd = vcarry.isadd = true;
		if (v2.isVariable())
			Helper.addCarryIdToVariable(vcarry.id, v2.id);
		for (int j = 1; j < Helper.BIT_NUMBER; j++) {
			resultString += toXor(vresult.id * Helper.BIT_NUMBER
					+ j, v2.id * Helper.BIT_NUMBER + j, v1.id
					* Helper.BIT_NUMBER + j, vcarry.id
					* Helper.BIT_NUMBER + j);

			resultString += toAtLeastTwo(vcarry.id
					* Helper.BIT_NUMBER + j + 1, v2.id
					* Helper.BIT_NUMBER + j, v1.id
					* Helper.BIT_NUMBER + j, vcarry.id
					* Helper.BIT_NUMBER + j);
			numberLine += 14;
		}
		// final xor (no atleasttwo) -> no 1+1 = 0
		resultString += toXor(vresult.id * Helper.BIT_NUMBER
				+ Helper.BIT_NUMBER, v2.id * Helper.BIT_NUMBER + Helper.BIT_NUMBER, v1.id
				* Helper.BIT_NUMBER + Helper.BIT_NUMBER, vcarry.id
				* Helper.BIT_NUMBER + Helper.BIT_NUMBER);
		
		resultString += String.format(" %d  %d  %d 0\n", (vresult.id * Helper.BIT_NUMBER
				+ Helper.BIT_NUMBER), -(v2.id * Helper.BIT_NUMBER + Helper.BIT_NUMBER), -(v1.id
				* Helper.BIT_NUMBER + Helper.BIT_NUMBER));
		
		resultString += "\n-" + (vcarry.id * Helper.BIT_NUMBER+1) + " 0";
		numberLine += 10;
		System.out.println("sonho:" + vcarry.id + " kq "
				+ vresult.id);
		return vresult;
	}
	
	public MathObject subtract(MathObject v1, MathObject v2) {
		Variable vresult = Helper.getVariable("ResultOf ("
				+ v2.name + "-"
				+ v1.name + ")");
		Variable vcarry = Helper.getVariable("CarryOf ("
				+ v2.name + "-"
				+ v1.name + ")");
		vresult.isadd = vcarry.isadd = true;
		if (v2.isVariable())
			Helper.addCarryIdToVariable(vcarry.id, v2.id);
		for (int j = 1; j < Helper.BIT_NUMBER; j++) {
			resultString += toXorSub(vresult.id * Helper.BIT_NUMBER
					+ j, v2.id * Helper.BIT_NUMBER + j, v1.id
					* Helper.BIT_NUMBER + j, vcarry.id
					* Helper.BIT_NUMBER + j);

			resultString += toSubCarry(vcarry.id
					* Helper.BIT_NUMBER + j + 1, v2.id
					* Helper.BIT_NUMBER + j, v1.id
					* Helper.BIT_NUMBER + j, vcarry.id
					* Helper.BIT_NUMBER + j);
			numberLine += 16;
		}
		
		// last toXorSub
		resultString += toXorSub(vresult.id * Helper.BIT_NUMBER
				+ Helper.BIT_NUMBER, v2.id * Helper.BIT_NUMBER + Helper.BIT_NUMBER, v1.id
				* Helper.BIT_NUMBER + Helper.BIT_NUMBER, vcarry.id
				* Helper.BIT_NUMBER + Helper.BIT_NUMBER);
		
		resultString += "\n-" + (vcarry.id * Helper.BIT_NUMBER+1) + " 0";
		numberLine += 9;
		System.out.println("nho(tru):" + vcarry.id + " kq(tru):"
				+ vresult.id);
		return vresult;
	}

	public ArrayList<MathObject> getOneSideExpressionCnf(
			ArrayList<MathObject> stackinput) {
		ArrayList<MathObject> stack = stackinput;
		int i = 0;
		while (i < stack.size() && stack.size() != 1) {
			MathObject m = stack.get(i);
			if (m instanceof Operator) {
				if (m.name.equals("+")) {
					MathObject vresult=add(stack.get(i - 1), stack.get(i - 2));
					stack.remove(i - 2);
					stack.remove(i - 2);
					stack.set(i - 2, vresult);
					i = 0;
				}
				else if (m.name.equals("-")) {
					MathObject vresult=subtract(stack.get(i-1), stack.get(i-2));
					stack.remove(i - 1);
					stack.remove(i - 2);
					stack.set(i - 2, vresult);
					i = 0;
				}
				else if (m.name.equals("*")) {
					int id1 = stack.get(i - 2).id;
					int id2 = stack.get(i - 1).id;
					ArrayList<MathObject> Stack = new ArrayList<MathObject>();
					for (int j = 0; j < Helper.BIT_NUMBER; j++) {
						Variable newVariable = Helper.getVariable("("
								+ stack.get(i - 2).name + ")" + j);
						newVariable.isadd = true;
						resultString += toEqualDisjunctive(newVariable.id, id1,
								id2, j);
						Stack.add(newVariable);
					}
					while (Stack.size() != 2) {
						 System.out.println("truoc");
						 for(MathObject v:Stack)
						 System.out.println(v.name);
						MathObject vresult=add(Stack.get(Stack.size() - 1),Stack.get(Stack.size() - 2));
						Stack.remove(Stack.size() - 2);
						Stack.remove(Stack.size() - 1);
						Stack.add(vresult);
						 for(MathObject v:Stack)
						 System.out.println(v.name);
					}
					int id11 = Stack.get(0).id;
					int id22 = Stack.get(1).id;
					Variable vresult = Helper.getVariable("ResultOf ("
							+ stack.get(i - 2).name + "*"
							+ stack.get(i - 1).name + ")");

					Variable vcarry = Helper.getVariable("CarryOf ("
							+ stack.get(i - 2).name + "*"
							+ stack.get(i - 1).name + ")");
					vresult.isadd = vcarry.isadd = true;
					for (int j = 1; j < Helper.BIT_NUMBER; j++) {
						resultString += toXor(vresult.id * Helper.BIT_NUMBER
								+ j, id11 * Helper.BIT_NUMBER + j, id22
								* Helper.BIT_NUMBER + j, vcarry.id
								* Helper.BIT_NUMBER + j);
						resultString += toAtLeastTwo(vcarry.id
								* Helper.BIT_NUMBER + j + 1, id11
								* Helper.BIT_NUMBER + j, id22
								* Helper.BIT_NUMBER + j, vcarry.id
								* Helper.BIT_NUMBER + j);
						numberLine += 14;
					}
					resultString += toXor(vresult.id * Helper.BIT_NUMBER
							+ Helper.BIT_NUMBER, id11 * Helper.BIT_NUMBER + Helper.BIT_NUMBER, id22
							* Helper.BIT_NUMBER + Helper.BIT_NUMBER, vcarry.id
							* Helper.BIT_NUMBER + Helper.BIT_NUMBER);
					resultString += "\n-" + (vcarry.id * Helper.BIT_NUMBER+1)
							+ " 0";
					numberLine += 9;
					System.out.println("sonho:" + vcarry.id + " kq "
							+ vresult.id);
					stack.remove(i - 2);
					stack.remove(i - 2);
					stack.set(i - 2, vresult);
					i = 0;

				}
			}
			i++;
		}
		return stack;
	}

	public String getCnf() {
		if (postfix1.size() == 0 || postfix2.size() == 0)
			return null;
		resultString = "";
		numberLine = 0;
		//get cnf left expression
		ArrayList<MathObject> stack1 = getOneSideExpressionCnf(postfix1);
		//get cnf right expression
		ArrayList<MathObject> stack2 = getOneSideExpressionCnf(postfix2);
		
		String result = resultString;
		//cnf equal left expression with right expression
		for (int j = 1; j <= Helper.BIT_NUMBER; j++) {

			result += "\n-" + (stack1.get(0).id * Helper.BIT_NUMBER + j) + " "
					+ (stack2.get(0).id * Helper.BIT_NUMBER + j) + " 0";
			result += "\n" + (stack1.get(0).id * Helper.BIT_NUMBER + j) + " -"
					+ (stack2.get(0).id * Helper.BIT_NUMBER + j) + " 0";
		}
		numberLine += Helper.BIT_NUMBER * 2;
		return result;
	}

	// output= Bi "^" Ci (Disjunctive) i=level
	private String toEqualDisjunctive(int output, int b, int c, int level) {
		String result = "";
		level++;
		for (int i = 1; i < level; i++) {
			result += "\n-" + (output * Helper.BIT_NUMBER + i) + " 0";
			numberLine += 1;
		}
		int k = 1;
		for (int i = level; i <= Helper.BIT_NUMBER; i++) {
			result += String.format("\n%d %d 0", b * Helper.BIT_NUMBER + k,
					-(output * Helper.BIT_NUMBER + i));
			result += String.format("\n%d %d 0", c * Helper.BIT_NUMBER + level,
					-(output * Helper.BIT_NUMBER + i));
			result += String.format("\n%d %d %d 0",
					-(b * Helper.BIT_NUMBER + k),
					-(c * Helper.BIT_NUMBER + level), output
							* Helper.BIT_NUMBER + i);
			numberLine += 3;
			k++;
		}
		return result;
	}

	// input=1 <=> b or c or d =1, or b=c=d=1
	private String toXor(int input, int b, int c, int d) {
		String result = "";
		result += String.format("\n %d %d  %d  %d 0\n", input, -b, c, d);
		result += String.format(" %d  %d %d  %d 0\n", input, b, -c, d);
		result += String.format(" %d  %d  %d %d 0\n", input, b, c, -d);
		result += String.format("%d %d %d  %d 0\n", -input, -b, -c, d);
		result += String.format("%d %d  %d %d 0\n", -input, -b, c, -d);
		result += String.format("%d  %d %d %d 0\n", -input, b, -c, -d);
		result += String.format("%d  %d  %d  %d 0\n", -input, b, c, d);
		result += String.format("%d  %d  %d  %d 0", input, -b, -c, -d);
		return result;
	}

	private String toXorSub(int input, int b, int c, int d) {
		String result = "";
		result += String.format("\n%d %d  %d  %d 0\n", input, -b, -c, -d);
		result += String.format("%d  %d %d  %d 0\n", -input, -b, -c, d);
		result += String.format("%d  %d  %d %d 0\n", -input, -b, c, -d);
		result += String.format(" %d %d %d  %d 0\n", input, -b, c, d);
		result += String.format(" %d %d  %d %d 0\n", -input, b, -c, -d);
		result += String.format(" %d  %d %d %d 0\n", input, b, -c, d);
		result += String.format("%d  %d  %d %d 0\n", input, b, c, -d);
		result += String.format(" %d  %d  %d  %d 0", -input, b, c, d);
		return result;
	}

	private String toAtLeastTwo(int input, int a, int b, int c) {
		String result = "";
		result += String.format("\n%d  %d  %d 0\n", -input, a, b);
		result += String.format("%d  %d  %d 0\n", -input, a, c);
		result += String.format("%d  %d  %d 0\n", -input, b, c);
		result += String.format(" %d %d %d 0\n", input, -a, -b);
		result += String.format(" %d %d %d 0\n", input, -a, -c);
		result += String.format(" %d %d %d 0", input, -b, -c);
		return result;
	}
	
	// b - c nho d
	private String toSubCarry(int input, int b, int c, int d) {
		String result = "";
		result += String.format("\n%d %d  %d  %d 0\n", input, -b, -c, -d);
		result += String.format("%d  %d %d  %d 0\n", -input, -b, -c, d);
		result += String.format("%d  %d  %d %d 0\n", -input, -b, c, -d);
		result += String.format(" %d %d %d  %d 0\n", -input, -b, c, d);
		result += String.format(" %d %d  %d %d 0\n", input, b, -c, -d);
		result += String.format(" %d  %d %d %d 0\n", input, b, -c, d);
		result += String.format(" %d  %d  %d  %d 0\n", input, b, c, -d);
		result += String.format(" %d  %d  %d  %d 0", -input, b, c, d);
		return result;
	}
}