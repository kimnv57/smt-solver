package math;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Stack;

import variable.Helper;

/**
 * some math tool
 * 
 * 
 */
public class Utils {

//	public static void main(String[] args) {
//		long a = 5;
//		long b = 39;
//		long[] r = Congruences.extendedEuclidean(a, b);
//
//		System.out.printf(checkExpression("A+b=c") + "");
//	}

	// Ham kiem tra xem phep tinh da dung chuan hay chua
	public static boolean checkExpression(String expression) {
		try {
			expression=expression.replaceAll(" ", "");
			String dau = new String("+-*^=><");
			String bieuThuc = "+-*^><=1234567890()";

			int length = expression.length();
			for (int i = 0; i < length; i++)
				if ((expression.charAt(i) < 'a' || expression.charAt(i) > 'z')
						&& (expression.charAt(i) < 'A' || expression.charAt(i) > 'Z'))
					if (bieuThuc.indexOf(expression.charAt(i)) < 0)
						return false;
			if (!checkPhepTinh(expression))
				return false;
			if (!checkDauNgoac(expression))
				return false;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static class Congruences {
		public static long[] extendedEuclidean(long a, long b) {
			long s0 = 1, s1 = 0, q;
			Stack<Long> Q = new Stack<Long>();

			while (b != 0) {
				Q.push(a / b);
				q = a % b;
				a = b;
				b = q;
			}
			while (!Q.isEmpty()) {

				q = s0 - s1 * Q.pop();
				s0 = s1;
				s1 = q;
			}
			return new long[] { a, s0, s1 };
		}
	}

	public static long gcd(long a, long b) {
		if (a < 0)
			a = -a;
		if (b < 0)
			b = -b;
		while (b != 0) {
			a %= b;
			if (a == 0)
				return b;
			b %= a;
		}
		return a;
	}

	public static long sqrt(long x) {
		long d1 = 1, d2 = x / d1, y = (d1 + d2) / 2;

		while (y != d1 && y != d2) {
			d1 = y;
			d2 = x / d1;
			y = (d1 + d2) / 2;
		}
		return y;
	}

	public static BigInteger factorial(long n) {
		BigInteger N = BigInteger.ONE;

		for (long i = 2; i <= n; i++)
			N = N.multiply(BigInteger.valueOf(i));
		return N;
	}

	// ham dem so lan xuat hien cua 1 ki tu trong chuoi string
	private static int demSoLanXuatHien(String s, char kiTu) {
		int num = 0;
		int length = s.length();
		for (int i = 0; i < length; i++)
			if (s.charAt(i) == kiTu)
				num++;
		return num;
	}

	// ham kiem tra xem cac dau phap tinh, cac bien da dung chuan hay chua
	private static boolean checkPhepTinh(String expression) {
		String dau = new String("+-*=^><");
		String number = "0123456789";
		String chuCai = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
		int length = expression.length();
		if (dau.indexOf(expression.charAt(0)) >= 0
				|| dau.indexOf(expression.charAt(length - 1)) >= 0)
			return false;
		for (int i = 1; i < length - 1; i++) {
			if (dau.indexOf(expression.charAt(i)) >= 0
					&& dau.indexOf(expression.charAt(i + 1)) >= 0) {
				if ((expression.charAt(i) == '>' || expression.charAt(i) == '<')
						&& expression.charAt(i + 1) == '=')
					continue;
				else
					return false;
			}
		}
		// for(int i=0; i<length-1; i++)
		// {
		// if(chuCai.indexOf(expression.charAt(i))>=0 &&
		// chuCai.indexOf(expression.charAt(i+1))>=0)
		// return false;
		// if(chuCai.indexOf(expression.charAt(i))>=0 &&
		// number.indexOf(expression.charAt(i+1))>=0)
		// return false;
		// if(number.indexOf(expression.charAt(i))>=0 &&
		// chuCai.indexOf(expression.charAt(i+1))>=0)
		// return false;
		// }
		return true;

	}

	// kiem tra dau bieu thuc =, >, <, >=, =<
	private static boolean checkDauBieuThuc(String expression) {
		if (demSoLanXuatHien(expression, '=') > 1
				|| demSoLanXuatHien(expression, '>') > 1
				|| demSoLanXuatHien(expression, '<') > 1)
			return false;
		if (demSoLanXuatHien(expression, '>')
				+ demSoLanXuatHien(expression, '<') > 1)
			return false;
		return true;
	}

	// ham kiem tra xem dau ngoac da dung hay chua
	private static boolean checkDauNgoac(String expression) {
		String dau = new String("+-*^=<>");
		int length = expression.length();
		if (expression.charAt(length - 1) == '(')
			return false;
		if (expression.charAt(0) == ')')
			return false;
		for (int i = 1; i < length - 1; i++) {
			if (expression.charAt(i) == '('
					&& (dau.indexOf(expression.charAt(i - 1)) < 0 || dau
							.indexOf(expression.charAt(i + 1)) >= 0))
				return false;
			if (expression.charAt(i) == ')'
					&& (i == 0 || dau.indexOf(expression.charAt(i - 1)) >= 0 || dau
							.indexOf(expression.charAt(i + 1)) < 0))
				return false;
		}
		if (demSoLanXuatHien(expression, '(') != demSoLanXuatHien(expression,
				')'))
			return false;
		int index = expression.indexOf('=');
		if (index < 0)
			index = expression.indexOf('<');
		if (index < 0)
			index = expression.indexOf('>');
		String expression1 = expression.substring(0, index);
		String expression2 = expression.substring(index + 1, length);
		if (demSoLanXuatHien(expression1, '(') != demSoLanXuatHien(expression1,
				')'))
			return false;
		return true;
	}

	// binary

	public static String DecimalToBinary(String number) {
		BigInteger num1 = new BigInteger(number);
		String result = toRadix(num1, 2);
		while(result.length()<Helper.BIT_NUMBER) result="0"+result; 
		return result;
	}

	public static int DecimalToBinary(int number) {
		return number;
	}

	private static String toRadix(BigInteger number, int radix) {
		return number.toString(radix);
	}

	// convert a+b=c to (a+b)-(c)=0
	public static String toOneExpression(String expression) {
		String[] array = expression.split("=");
		if(array[1].equals("0")||array[1].equals(" 0")) {
			return "("+array[0]+")";
		}
		String result = "(" + array[0] + ")-(" + array[1] + ")";
		return result;
	}
}
