package math;

public interface Executeable {
	/**
	 * 
	 * @param elements
	 * @return
	 */
	public Object exec(Object... elements);
}