package math;
public abstract class MathObject {
	public int id;
	public String name;

	public boolean isadd;
	
	public boolean isObject(String compare) {
		if(compare.equals(name)) return true;
		return false;
	}
	
	public boolean isVariable() {
		if(isadd) return false;
		if(name.equals("")) return false;
		try {
			int a = Integer.valueOf(name);
			return false;
		}catch(Exception e) {
			return true;
		}
	}
	
}