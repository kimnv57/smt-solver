package math;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class GenerateExpressions {
	private ArrayList<String> ExpressionsArray;
	public GenerateExpressions() {
		ExpressionsArray=new ArrayList<String>();
		try {
			Scanner sc = new Scanner(new File("Expressions"));
			while(sc.hasNext()) {
				String expression= sc.nextLine();
				ExpressionsArray.add(expression);
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public String[] getRandomExpressions() {
		String[] array;
		Random rd = new Random();
		ArrayList<String> copyArray=(ArrayList<String>) ExpressionsArray.clone();
		int size=5;
		while(size>4) {
			size=rd.nextInt(ExpressionsArray.size()-1)+1;
		}
		array= new String[size];
		for(int i=0;i<size;i++) {
			int index=rd.nextInt(copyArray.size()-1)+1;
			String expression=copyArray.get(index);
			copyArray.remove(index);
			array[i]=expression;
		}
		return array;
	}
}
