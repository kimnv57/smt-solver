package variable;

import java.util.ArrayList;

import math.Variable;

public class Helper {
	public static int BIT_NUMBER=8;
	public static int id=-1;
	public static ArrayList<Variable> varList = new ArrayList<Variable>();
	public static ArrayList<Integer> resultList = new ArrayList<Integer>();
	public static String fileContent="";
	public static int NUMBER_COLUM_PATH=4;
	public static int NUMBER_ROW_PATH=300;

	// Check if variable exist, if not -> create new
	public static Variable getVariable(String cVar) {
		for (Variable i : varList) {
			if (i.isObject(cVar)) {
				return i;
			}
		}
		Variable newVar = new Variable(Helper.id++,cVar);
		varList.add(newVar);
		return newVar;
	}
	
	public static ArrayList<Variable> getListVariable() {
		return varList;
	}

	public static void addCarryIdToVariable(int id2, int id1) {
		for(Variable v: varList) {
			if(v.id==id1) {
				v.carryid=id2;
				return;
			}
		}
	}
}

