package graphics;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

import math.Expression;


/*
 * smt panel content list expression
 */
public class SMTPanel extends JPanel {
	public ArrayList<Expression> listExpression;
	private final int paddingX=60;
	private final int paddingY=40;
	private final int marginY=40;
	private final int lengthText=19;

	/**
	 * Create the panel.
	 */
	public SMTPanel() {
		super();
		setLayout(null);
	}
	
	public void createGraphFromExpressionList(ArrayList<Expression> listExpression) {
		this.listExpression=listExpression;
		repaint();
		
	}


	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setFont(new Font("Arial", Font.PLAIN, 36));
		if(listExpression!=null) {
			int d=0;
			int maxStringLength=0;
			for(int i=0;i<listExpression.size();i++) {
				String content=listExpression.get(i).toString();
				if(content.length()>maxStringLength) {
					maxStringLength=content.length();
				}
				g2.drawString(content, paddingX,paddingY+d);
				d+=marginY;
			}
			if(listExpression.size()>1) 
				drawBracket(g2, listExpression.size());
		
			setPreferredSize(new Dimension(maxStringLength*lengthText+paddingX, paddingY+d));
			this.revalidate();
		}
		
	}
	
	public void drawBracket(Graphics2D g,int size) {
		int d=10;
		int midY= paddingY+  (marginY*(size-1)-10)/2;
		int firstY= paddingY-8;
		int lastY= paddingY+marginY*(size-1)-2;
		//vi tri x cua duong ke
		int x=paddingX-d;
		if(size>3) {
			g.drawLine(x,firstY, x, midY-5);
			g.drawLine(x, midY+5, x,lastY);
			g.drawLine(x, midY-5, x-7, midY);
			g.drawLine(x, midY+5, x-7, midY);
		}else {
			
			g.drawLine(x,firstY, x, midY-4);
			g.drawLine(x, midY+4, x,lastY);
			g.drawLine(x, midY-4, x-6, midY);
			g.drawLine(x, midY+4, x-6, midY);
			//g.drawLine(paddingX-d, paddingY, paddingX-d, paddingY+marginY*(size-1)-10);
		}
		g.drawLine(x, firstY, x+3, firstY-3);
		g.drawLine(x, lastY, x+3, lastY+3);
	}

}
