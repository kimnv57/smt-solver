package graphics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;

import variable.Helper;

public class test {
	public static void main(String[] args) throws Exception {
		ISolver solver = SolverFactory.newDefault();
		solver.setTimeout(3600);
		solver.setDBSimplificationAllowed(true);
		Reader reader = new DimacsReader(solver);
		IProblem problem = reader.parseInstance("filecnf.txt");
		if (problem.isSatisfiable()) {
			System.out.println("Satisfiable !");
			System.out.println(reader.decode(problem.model()));
		} else {
			System.out.println("Unsatisfiable !");
		}
	}

}