package graphics;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Timer;


public abstract class MyMouseAdapter extends MouseAdapter implements ActionListener {
	public static final int DELAY = 120;
	private Timer timer;

	{
		timer = new Timer(DELAY, this);
		timer.setInitialDelay(0);
	}
	
	public final void mousePressed(MouseEvent e) {
	    timer.start();
	}

	public final void mouseReleased(MouseEvent e) {
	    timer.stop();
	}
}
