package graphics;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import java.awt.SystemColor;

import javax.swing.SpringLayout;
import javax.swing.JTextPane;
import javax.swing.JTabbedPane;

import java.awt.Font;

import javax.swing.JTable;

import math.Utils;
import math.Variable;
import variable.Helper;

public class CNFPanel extends JPanel {
	private JTable table;
	private JTextPane textPanelResult;
	private JTextPane textPaneCnf;

	/**
	 * Create the panel.
	 */
	public CNFPanel() {
		super();
		setUi();
	}

	public void showUnsat() {
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			DefaultTableModel tm = (DefaultTableModel) table.getModel();
			updateRow(i, new String[] { "", "", "", "" }, tm);
		}
		textPanelResult.setText("Unsatisfiable !");
	}

	public void showData() {
		String result = "Result: ";
		for (Integer i : Helper.resultList) {
			if (i < 0)
				result += "\n\t" + i;
			else
				result += "\n\t " + i;
		}
		System.out.println(result);
		textPanelResult.setText(result);
		textPaneCnf.setText(Helper.fileContent);
		for (int i = 1; i < Helper.varList.size(); i++) {
			Variable v = Helper.varList.get(i);
			String id = v.id + "";
			String name = v.name;
			String value = "";
			String binary = "";
			if (v.isNumber() || v.isVariable()) {
				value = v.value + "";
				binary = Utils.DecimalToBinary(value);
			}
			if(v.isadd) {
				binary=v.binaryValue;
			}
			DefaultTableModel tm = (DefaultTableModel) table.getModel();
			updateRow(i, new String[] { id, name, value, binary }, tm);

		}
	}

	public void reset() {
		textPanelResult.setText("");
		textPaneCnf.setText("");
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			for (int j = 0; j < Helper.NUMBER_COLUM_PATH; j++) {
				((DefaultTableModel) table.getModel()).setValueAt("", i, j);
			}
			
		}
	}

	private void setUi() {
		setBackground(new Color(135, 206, 250));

		JLabel lblCnfProblem = new JLabel("CNF problem");

		JLabel lblSolution = new JLabel("Solution");

		JScrollPane scrollPane = new JScrollPane();

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_CnfPanel = new GroupLayout(this);
		gl_CnfPanel.setHorizontalGroup(
			gl_CnfPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_CnfPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_CnfPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_CnfPanel.createSequentialGroup()
							.addComponent(lblCnfProblem, GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblSolution, GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
						.addGroup(gl_CnfPanel.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE))))
		);
		gl_CnfPanel.setVerticalGroup(
			gl_CnfPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_CnfPanel.createSequentialGroup()
					.addGap(6)
					.addGroup(gl_CnfPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSolution, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCnfProblem, GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_CnfPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE))
					.addContainerGap())
		);

		JScrollPane scrollPane_1 = new JScrollPane();
		tabbedPane.addTab("result model", null, scrollPane_1, null);

		textPanelResult = new JTextPane();
		textPanelResult.setFont(new Font("Tahoma", Font.PLAIN, 14));
		scrollPane_1.setViewportView(textPanelResult);

		JScrollPane scrollPane_2 = new JScrollPane();
		tabbedPane.addTab("Variable", null, scrollPane_2, null);

		table = new JTable();
		scrollPane_2.setViewportView(table);

		textPaneCnf = new JTextPane();
		textPaneCnf.setFont(new Font("Tahoma", Font.PLAIN, 14));
		scrollPane.setViewportView(textPaneCnf);
		setLayout(gl_CnfPanel);
		table.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {

		}, new String[] { "ID", "Name", "Int", "BinaryValue" }) {
			Class[] types = new Class[] { java.lang.String.class,
					java.lang.String.class, java.lang.String.class,
					java.lang.String.class };

		});

		String[] o = new String[Helper.NUMBER_COLUM_PATH];
		for (int i = 0; i < o.length; i++) {
			o[i] = "";
		}
		for (int i = 0; i < Helper.NUMBER_ROW_PATH; i++) {
			((DefaultTableModel) table.getModel()).addRow(o);
		}
		if (table.getColumnModel().getColumnCount() > 0) {
			table.getColumnModel().getColumn(0).setMinWidth(10);
			table.getColumnModel().getColumn(0).setMaxWidth(60);
			table.getColumnModel().getColumn(0).setPreferredWidth(30);
			table.getColumnModel().getColumn(1).setMinWidth(10);
			table.getColumnModel().getColumn(1).setMaxWidth(150);
			table.getColumnModel().getColumn(1).setPreferredWidth(125);
			table.getColumnModel().getColumn(2).setMinWidth(10);
			table.getColumnModel().getColumn(2).setMaxWidth(150);
			table.getColumnModel().getColumn(2).setPreferredWidth(40);
		}

	}

	private void updateRow(int row, String[] o, DefaultTableModel tm) {
		for (int colum = 0; colum < o.length; colum++) {
			tm.setValueAt(o[colum], row, colum);
		}
	}
}
