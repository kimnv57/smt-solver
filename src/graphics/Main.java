package graphics;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import math.Expression;
import math.GenerateExpressions;
import math.SmtSolver;
import math.Utils;
import math.Variable;

import javax.swing.JTextPane;

import org.sat4j.specs.ContradictionException;

import variable.Helper;

import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JList;
import javax.swing.JComboBox;

public class Main {
	SmtSolver mySmtSolver;
	GenerateExpressions randomExpressions;
	ArrayList<String> StringExpression = new ArrayList<String>();

	private JFrame frame;
	private CNFPanel CnfPanel;
	private SMTPanel SmtPanel;
	private JTextField myTextField;
	private JButton btnAdd;
	private JButton btnSolver;
	private JTextPane textPanelResult;
	private JTextPane textPanelNote;
	private boolean runSmt = false;

	private AbstractButton btnClear;
	private JButton btnRandom;
	private JButton btnExit;
	private JDialog dialog;
	private JComboBox comboBox;

	/**
	 * Create the application.
	 */
	public Main() {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		initialize();
		create();
		setListener();
	}

	private void create() {
		mySmtSolver = new SmtSolver();
		randomExpressions = new GenerateExpressions();
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void setWaitDilog() {
		dialog = new JDialog(frame, "Dialog", ModalityType.APPLICATION_MODAL);
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.add(progressBar, BorderLayout.CENTER);
		panel1.add(new JLabel("Please wait......."), BorderLayout.PAGE_START);
		dialog.getContentPane().add(panel1);
		dialog.pack();
		dialog.setLocationRelativeTo(frame);
	}

	private void setListener() {
		comboBox.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(comboBox.getSelectedItem());
				Helper.BIT_NUMBER = Integer.valueOf(comboBox
						.getSelectedItem().toString());
				runSmt=false;
				
			}
		});
		btnAdd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (myTextField.getText() == null) {
					textPanelNote.setForeground(Color.RED);
					textPanelNote.setText("Expression can be null");
					return;
				}

				if (myTextField.getText().equals("")) {
					textPanelNote.setForeground(Color.RED);
					textPanelNote.setText("Expression can be null");
					return;
				}
				if (!Utils.checkExpression(myTextField.getText().toString())) {
					textPanelNote.setForeground(Color.RED);
					textPanelNote.setText(myTextField.getText().toString()
							+ " is not  an expression\nfor example a+b=8");
					return;
				}
				try {
					String expression = myTextField.getText();
					myTextField.setText("");
					StringExpression.add(expression);
					mySmtSolver = new SmtSolver();
					for (String exp : StringExpression) {
						Expression e = new Expression(exp);
						mySmtSolver.addExpression(e);
					}

					SmtPanel.createGraphFromExpressionList(mySmtSolver
							.getListExpression());
					textPanelNote.setForeground(Color.black);
					textPanelNote.setText("");
					runSmt = false;
				} catch (Exception ex) {
					textPanelNote.setForeground(Color.RED);
					textPanelNote.setText(ex.getMessage());
					ex.printStackTrace();
				}
			}
		});

		btnSolver.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (runSmt)
					return;
				if (mySmtSolver.isEmpty())
					return;
				setWaitDilog();
				SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {

					@Override
					protected String doInBackground() throws Exception {
						try {
							reset1();
							Helper.BIT_NUMBER = Integer.valueOf(comboBox
									.getSelectedItem().toString());
							if (mySmtSolver.run()) {
								runSmt = true;
								String result = "satisfiable !";
								for (Variable v : Helper.varList) {
									if (v.isVariable())
										result += "\n" + v.name + "= "
												+ v.value;
								}
								textPanelResult.setText(result);
								CnfPanel.showData();
								textPanelNote.setForeground(Color.blue);
								textPanelNote
										.setText("time running: "
												+ mySmtSolver.totalTime
												+ "\ntotal variable: "
												+ (Helper.id
														* Helper.BIT_NUMBER + Helper.BIT_NUMBER));
							} else {
								CnfPanel.showUnsat();
								textPanelResult.setText("Unsatisfiable !");
							}

						} catch (ContradictionException e) {
							CnfPanel.showData();
							CnfPanel.showUnsat();
							textPanelResult.setText("Unsatisfiable !");
							runSmt = true;
						} catch (Exception ex) {
							textPanelNote.setForeground(Color.RED);
							textPanelNote.setText(ex.getMessage());
							ex.printStackTrace();
						}
						return null;
					}

					@Override
					protected void done() {
						dialog.dispose();
					}

				};
				worker.execute();
				dialog.setVisible(true);

			}
		});

		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
			}
		});
		btnRandom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
				String[] Expressions = randomExpressions.getRandomExpressions();
				for (int i = 0; i < Expressions.length; i++) {
					StringExpression.add(Expressions[i]);
					Expression e = new Expression(Expressions[i]);
					mySmtSolver.addExpression(e);
				}
				SmtPanel.createGraphFromExpressionList(mySmtSolver
						.getListExpression());
			}
		});
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}

	private void reset() {
		textPanelNote.setForeground(Color.black);
		textPanelNote.setText("");
		textPanelResult.setForeground(Color.black);
		textPanelResult.setText("");
		Helper.varList = new ArrayList<Variable>();
		Helper.resultList = new ArrayList<Integer>();
		Helper.id = 0;
		mySmtSolver = new SmtSolver();
		SmtPanel.listExpression = new ArrayList<Expression>();
		StringExpression = new ArrayList<String>();
		SmtPanel.repaint();
		CnfPanel.reset();
		runSmt = false;
	}

	private void reset1() {
		textPanelNote.setForeground(Color.black);
		textPanelNote.setText("");
		textPanelResult.setForeground(Color.black);
		textPanelResult.setText("");
		CnfPanel.reset();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(20, 20, 600, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));

		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		frame.getContentPane().add(tabbedPane_1, "name_239533095747022");

		JPanel SMTpanel = new JPanel();
		SMTpanel.setBackground(new Color(135, 206, 235));
		tabbedPane_1.addTab("SMT", null, SMTpanel, null);

		JScrollPane scrollPane = new JScrollPane();

		JLabel lblNewLabel = new JLabel("Problem:");

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		GroupLayout gl_SMTpanel = new GroupLayout(SMTpanel);
		gl_SMTpanel
				.setHorizontalGroup(gl_SMTpanel
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_SMTpanel
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_SMTpanel
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_SMTpanel
																		.createSequentialGroup()
																		.addGroup(
																				gl_SMTpanel
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								scrollPane,
																								GroupLayout.DEFAULT_SIZE,
																								416,
																								Short.MAX_VALUE)
																						.addComponent(
																								panel_1,
																								GroupLayout.DEFAULT_SIZE,
																								416,
																								Short.MAX_VALUE))
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				panel,
																				GroupLayout.PREFERRED_SIZE,
																				141,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED))
														.addComponent(
																lblNewLabel,
																GroupLayout.PREFERRED_SIZE,
																426,
																GroupLayout.PREFERRED_SIZE))
										.addGap(2)));
		gl_SMTpanel
				.setVerticalGroup(gl_SMTpanel
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_SMTpanel
										.createSequentialGroup()
										.addComponent(lblNewLabel)
										.addGap(11)
										.addGroup(
												gl_SMTpanel
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																panel,
																GroupLayout.DEFAULT_SIZE,
																588,
																Short.MAX_VALUE)
														.addGroup(
																gl_SMTpanel
																		.createSequentialGroup()
																		.addComponent(
																				panel_1,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				scrollPane,
																				GroupLayout.DEFAULT_SIZE,
																				422,
																				Short.MAX_VALUE)))
										.addContainerGap()));

		btnSolver = new JButton("Solver");
		btnRandom = new JButton("Random");

		btnExit = new JButton("Exit");

		JScrollPane scrollPane_1 = new JScrollPane();

		btnClear = new JButton("clear");

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGap(18)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING,
																				false)
																				.addComponent(
																						btnExit,
																						Alignment.TRAILING,
																						GroupLayout.DEFAULT_SIZE,
																						71,
																						Short.MAX_VALUE)
																				.addComponent(
																						btnSolver,
																						Alignment.TRAILING,
																						GroupLayout.DEFAULT_SIZE,
																						71,
																						Short.MAX_VALUE)
																				.addComponent(
																						btnRandom,
																						Alignment.TRAILING,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						btnClear,
																						Alignment.TRAILING,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)))
												.addGroup(
														gl_panel.createSequentialGroup()
																.addContainerGap()
																.addComponent(
																		scrollPane_1,
																		GroupLayout.PREFERRED_SIZE,
																		125,
																		GroupLayout.PREFERRED_SIZE)))
								.addContainerGap(GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(btnSolver)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnRandom)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnClear)
						.addGap(11)
						.addComponent(btnExit)
						.addGap(26)
						.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE,
								403, Short.MAX_VALUE).addContainerGap()));

		textPanelResult = new JTextPane();
		textPanelResult.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textPanelResult.setEditable(false);
		scrollPane_1.setViewportView(textPanelResult);
		panel.setLayout(gl_panel);
		myTextField = new JTextField();
		myTextField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		myTextField.setBackground(SystemColor.inactiveCaptionBorder);
		myTextField.setColumns(10);

		btnAdd = new JButton("add");

		JLabel lblNewLabel_1 = new JLabel("Type Expression :");

		textPanelNote = new JTextPane();
		textPanelNote.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textPanelNote.setForeground(Color.BLACK);
		textPanelNote.setEditable(false);
		textPanelNote.setText("for example a+b=8");

		comboBox = new JComboBox(new String[] { "4", "6", "8", "10", "12",
				"14", "16","32","64" });
		comboBox.setSelectedItem("8");
		JLabel lblBitNumbers = new JLabel("Bit numbers");
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1
				.setHorizontalGroup(gl_panel_1
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								gl_panel_1
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.TRAILING)
														.addComponent(
																lblNewLabel_1,
																Alignment.LEADING)
														.addComponent(
																myTextField,
																Alignment.LEADING,
																GroupLayout.DEFAULT_SIZE,
																295,
																Short.MAX_VALUE)
														.addComponent(
																textPanelNote,
																Alignment.LEADING,
																GroupLayout.PREFERRED_SIZE,
																274,
																GroupLayout.PREFERRED_SIZE))
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnAdd)
																		.addGap(54))
														.addGroup(
																Alignment.TRAILING,
																gl_panel_1
																		.createSequentialGroup()
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				42,
																				Short.MAX_VALUE)
																		.addGroup(
																				gl_panel_1
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								lblBitNumbers)
																						.addComponent(
																								comboBox,
																								GroupLayout.PREFERRED_SIZE,
																								38,
																								GroupLayout.PREFERRED_SIZE))
																		.addGap(23)))));
		gl_panel_1
				.setVerticalGroup(gl_panel_1
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_1
										.createSequentialGroup()
										.addGap(6)
										.addComponent(lblNewLabel_1)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																myTextField,
																GroupLayout.PREFERRED_SIZE,
																35,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																btnAdd,
																GroupLayout.PREFERRED_SIZE,
																35,
																GroupLayout.PREFERRED_SIZE))
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				textPanelNote,
																				GroupLayout.PREFERRED_SIZE,
																				69,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addGap(18)
																		.addComponent(
																				lblBitNumbers)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				comboBox,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(22, Short.MAX_VALUE)));
		panel_1.setLayout(gl_panel_1);

		SmtPanel = new SMTPanel();
		SmtPanel.setBackground(SystemColor.inactiveCaptionBorder);
		scrollPane.setViewportView(SmtPanel);
		SmtPanel.setLayout(null);
		SMTpanel.setLayout(gl_SMTpanel);

		CnfPanel = new CNFPanel();
		tabbedPane_1.addTab("CNF", null, CnfPanel, null);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu FileMenu = new JMenu("File");
		menuBar.add(FileMenu);

		JMenuItem mntmOpen = new JMenuItem("open");
		FileMenu.add(mntmOpen);
	}
}
